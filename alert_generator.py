from collections import defaultdict, deque
from datetime import datetime, timedelta
from typing import Union, cast

from types_module import (
    Alert,
    LimitKey,
    OperatorFunc,
    SatelliteData,
    TelemetryData,
    TriggerConfig,
)


def evaluate_triggers(
    data: SatelliteData, satellite_id: int, component: str, timestamp: str, triggers: dict[str, TriggerConfig]
) -> Union[str, None]:
    """
    Evaluate if a given satellite data record triggers an alert.

    Parameters:
    - data: The telemetry data from satellites.
    - satellite_id: ID of the satellite.
    - component: Satellite component being checked.
    - timestamp: Timestamp of the data record.
    - triggers: Dictionary of trigger configurations.

    Returns:
    - Alarm severity string if record triggers an alert, otherwise None.
    """
    # Map operator string to the corresponding function
    operator_map: dict[str, OperatorFunc] = {
        ">": lambda x, y: x > y,
        "<": lambda x, y: x < y,
    }

    # Retrieve the current trigger definition for the given component
    trigger = triggers.get(component, None)
    if trigger:
        component_data: TelemetryData = data[satellite_id][component][timestamp]
        limit_value: float = component_data[cast(LimitKey, trigger["limit"])]
        operator: str = trigger["operator"]
        raw_value: float = component_data["raw_value"]
        severity: str = trigger["severity"]

        # Check if the raw value meets the trigger condition
        if operator_map[operator](raw_value, limit_value):
            return severity

    return None


def generate_alerts(data: SatelliteData, triggers: dict[str, TriggerConfig]) -> list[Alert]:
    """
    Generate a list of alerts based on satellite data and trigger conditions.

    Parameters:
    - data: The telemetry data from satellites.
    - triggers: Dictionary of trigger configurations.

    Returns:
    - A sorted list of alerts triggered.
    """
    alerts: list[Alert] = []

    # Iterate over all satellite data
    for satellite in data:
        for component in data[satellite]:
            for timestamp in data[satellite][component]:
                severity = evaluate_triggers(data, satellite, component, timestamp, triggers)
                if severity:
                    alerts.append(
                        {"satelliteId": satellite, "severity": severity, "component": component, "timestamp": timestamp}
                    )

    # Sort alerts by satellite ID, severity, component, and timestamp
    alerts.sort(key=lambda x: (x["satelliteId"], x["severity"], x["component"], x["timestamp"]))
    return alerts


def filter_alerts(alerts: list[Alert], triggers: dict[str, TriggerConfig]) -> list[Alert]:
    """
    Filter alerts based on the frequency and interval specified in trigger configurations.

    Parameters:
    - alerts: List of triggered alerts.
    - triggers: Dictionary of trigger configurations.

    Returns:
    - A list of filtered alerts adhering to frequency and interval constraints.
    """
    # Store the deque of timestamps for each (satellite, severity, component) combination
    timestamp_queues: dict[tuple[int, str, str], deque] = defaultdict(deque)
    filtered_alerts: list[Alert] = []

    for entry in alerts:
        key: tuple[int, str, str] = (entry["satelliteId"], entry["severity"], entry["component"])
        timestamp: datetime = datetime.strptime(entry["timestamp"], "%Y%m%d %H:%M:%S.%fZ")
        interval: timedelta = timedelta(minutes=triggers[entry["component"]]["intervalMinutes"])
        frequency: int = triggers[entry["component"]]["frequency"]

        # Append current timestamp to the queue for the given satellite, severity, component combination
        timestamp_queues[key].append(timestamp)

        # Remove oldest timestamps until the time span in the deque is within the allowed interval
        while timestamp - timestamp_queues[key][0] > interval:
            timestamp_queues[key].popleft()

        # Add an alert if the number of occurrences within the interval meets or exceeds the frequency constraint
        if len(timestamp_queues[key]) >= frequency:
            earliest_timestamp_str: str = timestamp_queues[key][0].strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3] + "Z"
            earliest_alert: Alert = {
                "satelliteId": key[0],
                "severity": key[1],
                "component": key[2],
                "timestamp": earliest_timestamp_str,
            }
            if earliest_alert not in filtered_alerts:
                filtered_alerts.append(earliest_alert)

    return filtered_alerts
