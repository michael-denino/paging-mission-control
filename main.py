import json
import logging
from pprint import pformat

from alert_generator import filter_alerts, generate_alerts
from data_loader import load_alert_triggers, load_satellite_data, parse_args
from types_module import Alert, SatelliteData, TriggerConfig

# Initialize logging configuration if needed (uncomment the line below to enable logging)
# logging.basicConfig(level=logging.INFO)


def main(args: dict) -> list[Alert]:
    """
    Orchestrates the process of loading satellite data, generating alerts based on it,
    and printing filtered alerts to stdout.

    Args:
        args (dict): A dictionary containing the paths for the input data and, optionally,
                     the alert triggers configuration files. Expected keys are:
                     - "input_file_path" (str): path to the satellite data file.
                     - "alert_triggers_path" (str, optional): path to the alert triggers
                       configuration file. If not provided, a default path or behavior should
                       be implemented in the called functions.

    Returns:
        list[Alert]: A list of filtered alerts based on the loaded satellite data and trigger configurations.
    """
    # Load satellite data from the specified path
    data: SatelliteData = load_satellite_data(args["input_file_path"])
    # Use pfomrat to acccurately represent the satellite id type int
    logging.info("Loaded Satellite Data:\n%s", pformat(data, indent=4))

    # Load alert triggers from the specified configuration path
    alert_triggers: dict[str, TriggerConfig] = load_alert_triggers(args["alert_triggers_path"])
    logging.info("Loaded Alert Triggers:\n%s", json.dumps(alert_triggers, indent=4))

    # Generate alerts based on the loaded satellite data and trigger configurations
    alerts: list[Alert] = generate_alerts(data, alert_triggers)
    logging.info("Generated Alerts:\n%s", json.dumps(alerts, indent=4))

    # Filter the generated alerts based on frequency and interval conditions
    filtered_alerts: list[Alert] = filter_alerts(alerts, alert_triggers)
    logging.info("Filtered Alerts:")

    return filtered_alerts


# Entry point of the script
if __name__ == "__main__":
    print(json.dumps(main(parse_args()), indent=4))
