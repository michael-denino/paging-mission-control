import json
import logging
import sys
from argparse import ArgumentParser

from types_module import Record, SatelliteData, TelemetryData, TriggerConfig


def parse_args() -> dict[str, str]:
    """
    Parse command-line arguments and return a dictionary containing file paths.

    Returns:
        dict[str, str]: Dictionary with keys "input_file_path" and "alert_triggers_path",
                        representing paths to the input and triggers configuration files, respectively.
    """
    parser = ArgumentParser(description="Process input and trigger config file paths.")
    parser.add_argument("file_path", metavar="file_path", type=str, help="Path to the input file.")
    parser.add_argument(
        "--trigger_config_path",
        default="alert_triggers.json",
        type=str,
        help="Path to the trigger configuration file (optional; defaults to 'alert_triggers.json').",
    )
    args = parser.parse_args()
    return {"input_file_path": args.file_path, "alert_triggers_path": args.trigger_config_path}


def parse_line(line: str) -> Record:
    """
    Parse a line of telemetry data and return a Record.

    Args:
        line (str): A single line of telemetry data.

    Returns:
        Record: A tuple containing satellite ID, component, timestamp, and telemetry data.
                If parsing fails, returns a tuple of four None values.
    """
    # Number of expected fields in a telemetry line
    field_count: int = 8

    fields = line.strip().split("|")

    # Only process lines with the correct number of fields
    if len(fields) == field_count:
        # Append 'Z' to represent Zulu time (UTC)
        timestamp: str = fields[0] + "Z"
        satellite_id: int = int(fields[1])

        # Extract telemetry data from the line
        telemetry_data: TelemetryData = {
            "red_high_limit": int(fields[2]),
            "yellow_high_limit": int(fields[3]),
            "yellow_low_limit": int(fields[4]),
            "red_low_limit": int(fields[5]),
            "raw_value": float(fields[6]),
        }
        component: str = fields[7]

        return satellite_id, component, timestamp, telemetry_data

    # Return None values for lines with an incorrect number of fields
    return None, None, None, None


def load_satellite_data(file_path: str) -> SatelliteData:
    """
    Load satellite telemetry data from a file and return it as a dictionary.

    Args:
        file_path (str): The path to the file containing the satellite telemetry data.

    Returns:
        SatelliteData: A dictionary with the telemetry data, organized by satellite ID, component, and timestamp.

    Raises:
        SystemExit: Exits the script if the file is not found.
    """
    data_by_satellite: SatelliteData = {}
    try:
        with open(file_path, "r", encoding="ASCII") as file:
            for line in file:
                satellite_id, component, timestamp, telemetry_data = parse_line(line)

                # Skip lines with incomplete or missing data
                if not satellite_id or not component or not timestamp or not telemetry_data:
                    continue

                # Organize telemetry data by satellite ID, then by component, and finally by timestamp
                data_by_satellite.setdefault(satellite_id, {}).setdefault(component, {})[timestamp] = telemetry_data
        return data_by_satellite
    except FileNotFoundError:
        # Log an error and exit the script if the file is not found
        logging.error(f"File not found: {file_path}")
        sys.exit(1)


def load_alert_triggers(trigger_config_path: str) -> dict[str, TriggerConfig]:
    """
    Load trigger configurations from a JSON file.

    Args:
        trigger_config_path (str): The path to the JSON file containing trigger configurations.

    Returns:
        dict[str, TriggerConfig]: A dictionary containing the alert triggers for each component.

    Raises:
        SystemExit: Exits the script if the file is not found.
    """
    try:
        # Open and read the trigger configurations from the JSON file
        with open(trigger_config_path, "r") as file:
            triggers = json.load(file)
        return triggers
    except FileNotFoundError:
        # Log an error and exit the script if the file is not found
        logging.error(f"File not found: {trigger_config_path}")
        sys.exit(1)
