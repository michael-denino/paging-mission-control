from typing import Callable, Literal, Optional, TypedDict


class Alert(TypedDict):
    """
    Represents an alert triggered by the satellite telemetry data.

    Fields:
    - satelliteId: ID of the satellite that generated the alert.
    - severity: Severity level of the alert (e.g., 'red', 'yellow').
    - component: Satellite component responsible for the alert.
    - timestamp: Timestamp of when the alert was triggered.
    """

    satelliteId: int
    severity: str
    component: str
    timestamp: str


class TriggerConfig(TypedDict):
    """
    Configuration for the conditions that trigger an alert.

    Fields:
    - limit: The telemetry value limit type (e.g., 'red_high_limit', 'yellow_low_limit').
    - operator: Operator used for the triggering condition (e.g., '<', '>').
    - intervalMinutes: Time window (in minutes) for which the frequency condition applies.
    - frequency: Number of occurrences required within the interval to trigger an alert.
    - severity: Severity level to be assigned to the alert if triggered.
    """

    limit: str
    operator: str
    intervalMinutes: int
    frequency: int
    severity: str


class TelemetryData(TypedDict):
    """
    Telemetry data structure received from a satellite component.

    Fields:
    - red_high_limit: Upper threshold for critical alert.
    - yellow_high_limit: Upper threshold for warning alert.
    - yellow_low_limit: Lower threshold for warning alert.
    - red_low_limit: Lower threshold for critical alert.
    - raw_value: Actual telemetry value reported by the satellite component.
    """

    red_high_limit: float
    yellow_high_limit: float
    yellow_low_limit: float
    red_low_limit: float
    raw_value: float


# A set of valid keys corresponding to different telemetry limits and the raw value itself.
LimitKey = Literal["red_high_limit", "yellow_high_limit", "yellow_low_limit", "red_low_limit", "raw_value"]

# Type hint for a function that compares two floats using an operator and returns a boolean result.
OperatorFunc = Callable[[float, float], bool]

# A record containing optional satellite id, severity, component, and telemetry data.
Record = tuple[Optional[int], Optional[str], Optional[str], Optional[TelemetryData]]

# Nested dictionary representing telemetry data for multiple satellites and their components over time.
SatelliteData = dict[int, dict[str, dict[str, TelemetryData]]]
