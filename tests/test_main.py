import json
import unittest

from main import main
from types_module import Alert


class TestMain(unittest.TestCase):
    def test_main(self) -> None:
        """
        Test the main function with sample input and compare the output with expected results.
        """
        # Call the main function with test data and capture the output
        output: list[Alert] = main(
            {"input_file_path": "./test_data/input.txt", "alert_triggers_path": "./test_data/alert_triggers.json"}
        )

        # Load the expected output from a file
        with open("./test_data/expected_output.json") as f:
            expected_output = json.load(f)

        # Assert that the output matches the expected output
        self.assertEqual(output, expected_output)


if __name__ == "__main__":
    unittest.main()
