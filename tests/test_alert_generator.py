import unittest
from unittest.mock import MagicMock, patch

from alert_generator import evaluate_triggers, filter_alerts, generate_alerts
from types_module import Alert, SatelliteData, TriggerConfig


class TestAlertGeneratorFunctions(unittest.TestCase):
    data: SatelliteData = {
        1001: {
            "TSTAT": {
                "20180101 23:01:26.011Z": {
                    "red_high_limit": 101,
                    "yellow_high_limit": 98,
                    "yellow_low_limit": 25,
                    "red_low_limit": 20,
                    "raw_value": 101.5,
                }
            },
            "BATT": {
                "20180101 23:02:09.014Z": {
                    "red_high_limit": 50,
                    "yellow_high_limit": 40,
                    "yellow_low_limit": 5,
                    "red_low_limit": 10,
                    "raw_value": 45.0,
                }
            },
        }
    }

    @patch("alert_generator.cast")
    def test_evaluate_triggers(self, mock_cast: MagicMock) -> None:
        """Test the evaluate_triggers function."""
        # Mock the inputs and data for an alert that triggers
        satellite_id = 1001
        component = "TSTAT"
        timestamp = "20180101 23:01:26.011Z"
        triggers: dict[str, TriggerConfig] = {
            "TSTAT": {
                "limit": "red_high_limit",
                "operator": ">",
                "intervalMinutes": 5,
                "frequency": 3,
                "severity": "RED HIGH",
            },
            "BATT": {
                "limit": "red_low_limit",
                "operator": "<",
                "intervalMinutes": 5,
                "frequency": 3,
                "severity": "RED LOW",
            },
        }

        mock_cast.return_value = "red_high_limit"

        # Call the evaluate_triggers function
        result = evaluate_triggers(self.data, satellite_id, component, timestamp, triggers)

        # Assert that the result is the expected severity
        self.assertEqual(result, "RED HIGH")

    # Test cases for generate_alerts function
    def test_generate_alerts(self) -> None:
        """Test the generate_alerts function."""
        # Sample trigger configurations
        triggers: dict[str, TriggerConfig] = {
            "TSTAT": {
                "limit": "red_high_limit",
                "operator": ">",
                "intervalMinutes": 5,
                "frequency": 2,
                "severity": "RED HIGH",
            },
            "BATT": {
                "limit": "yellow_low_limit",
                "operator": "<",
                "intervalMinutes": 5,
                "frequency": 3,
                "severity": "YELLOW LOW",
            },
        }

        # Call the generate_alerts function
        alerts = generate_alerts(self.data, triggers)

        # Expected alerts based on trigger conditions
        expected_alerts = [
            {"satelliteId": 1001, "severity": "RED HIGH", "component": "TSTAT", "timestamp": "20180101 23:01:26.011Z"}
        ]

        # Assert that the generated alerts match the expected alerts
        self.assertEqual(alerts, expected_alerts)

    def test_filter_alerts(self) -> None:
        """Test the filter_alerts function."""
        # Define test data
        alerts: list[Alert] = [
            {"satelliteId": 1000, "severity": "RED HIGH", "component": "TSTAT", "timestamp": "20180101 23:01:26.011Z"},
            {"satelliteId": 1000, "severity": "RED HIGH", "component": "BATT", "timestamp": "20180101 23:02:09.014Z"},
            {"satelliteId": 1000, "severity": "RED HIGH", "component": "TSTAT", "timestamp": "20180101 23:03:03.008Z"},
        ]
        triggers: dict[str, TriggerConfig] = {
            "TSTAT": {
                "limit": "red_high_limit",
                "operator": ">",
                "intervalMinutes": 5,
                "frequency": 2,
                "severity": "RED HIGH",
            },
            "BATT": {
                "limit": "red_low_limit",
                "operator": "<",
                "intervalMinutes": 5,
                "frequency": 3,
                "severity": "RED LOW",
            },
        }

        # Call the filter_alerts function
        result = filter_alerts(alerts, triggers)

        # Define the expected result
        expected_result = [
            {"satelliteId": 1000, "severity": "RED HIGH", "component": "TSTAT", "timestamp": "2018-01-01T23:01:26.011Z"}
        ]

        # Assert that the result matches the expected result
        self.assertEqual(result, expected_result)


if __name__ == "__main__":
    unittest.main()
