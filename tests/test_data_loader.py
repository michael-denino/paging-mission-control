import argparse
import unittest
from unittest.mock import MagicMock, mock_open, patch

from data_loader import load_alert_triggers, load_satellite_data, parse_args, parse_line


class TestDataLoaderFunctions(unittest.TestCase):
    def test_parse_args(self) -> None:
        """Test the parse_args function."""
        # Mock the argparse namespace
        with patch(
            "argparse.ArgumentParser.parse_args",
            return_value=argparse.Namespace(
                file_path="sample_input_path.txt", trigger_config_path="alert_triggers.json"
            ),
        ):
            args = parse_args()

        self.assertEqual(
            args, {"input_file_path": "sample_input_path.txt", "alert_triggers_path": "alert_triggers.json"}
        )

    def test_parse_line(self) -> None:
        """Test the parse_line function."""
        # Define the sample telemetry data
        sample_data = [
            "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT",
            "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT",
        ]

        # Define the expected results
        expected_results = [
            (
                1000,
                "BATT",
                "20180101 23:01:09.521Z",
                {
                    "red_high_limit": 17,
                    "yellow_high_limit": 15,
                    "yellow_low_limit": 9,
                    "red_low_limit": 8,
                    "raw_value": 7.8,
                },
            ),
            (
                1001,
                "TSTAT",
                "20180101 23:01:26.011Z",
                {
                    "red_high_limit": 101,
                    "yellow_high_limit": 98,
                    "yellow_low_limit": 25,
                    "red_low_limit": 20,
                    "raw_value": 99.8,
                },
            ),
        ]

        # Loop through the sample data and expected results
        for line, expected_result in zip(sample_data, expected_results):
            # Call the parse_line function on the input line
            result = parse_line(line)

            # Assert that the result matches the expected result
            self.assertEqual(result, expected_result)

    @patch(
        "builtins.open",
        new_callable=mock_open,
        read_data=(
            "20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT\n"
            "20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT\n"
            "20180101 23:01:26.011|1001|101|98|25|20|99.8|TSTAT\n"
        ),
    )
    def test_load_satellite_data(self, mock_file: MagicMock) -> None:
        """Test the load_satellite_data function."""
        # Define the expected result as a dictionary
        expected_data = {
            1001: {
                "TSTAT": {
                    "20180101 23:01:05.001Z": {
                        "red_high_limit": 101,
                        "yellow_high_limit": 98,
                        "yellow_low_limit": 25,
                        "red_low_limit": 20,
                        "raw_value": 99.9,
                    },
                    "20180101 23:01:26.011Z": {
                        "red_high_limit": 101,
                        "yellow_high_limit": 98,
                        "yellow_low_limit": 25,
                        "red_low_limit": 20,
                        "raw_value": 99.8,
                    },
                }
            },
            1000: {
                "BATT": {
                    "20180101 23:01:09.521Z": {
                        "red_high_limit": 17,
                        "yellow_high_limit": 15,
                        "yellow_low_limit": 9,
                        "red_low_limit": 8,
                        "raw_value": 7.8,
                    }
                }
            },
        }

        # Call the function to load satellite data
        result = load_satellite_data("sample_data.txt")

        # Assert that the loaded data matches the expected data
        self.assertEqual(result, expected_data)

    @patch(
        "builtins.open",
        new_callable=mock_open,
        read_data=(
            '{"TSTAT": {"red_high_limit": 100, "yellow_high_limit": 90, '
            '"red_low_limit": 10, "yellow_low_limit": 20}, '
            '"BATT": {"red_high_limit": 50, "yellow_high_limit": 40, '
            '"red_low_limit": 5, "yellow_low_limit": 10}}'
        ),
    )
    def test_load_alert_triggers(self, mock_file: MagicMock) -> None:
        """Test the load_alert_triggers function."""
        # Define the expected result as a dictionary
        expected_triggers = {
            "TSTAT": {"red_high_limit": 100, "yellow_high_limit": 90, "red_low_limit": 10, "yellow_low_limit": 20},
            "BATT": {"red_high_limit": 50, "yellow_high_limit": 40, "red_low_limit": 5, "yellow_low_limit": 10},
        }
        # Call the function to load alert triggers
        result = load_alert_triggers("alert_triggers.json")
        # Assert that the loaded triggers match the expected triggers
        self.assertEqual(result, expected_triggers)


if __name__ == "__main__":
    unittest.main()
